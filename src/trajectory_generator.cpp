#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "rclcpp/rclcpp.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

// Trajectory generator node 
class TrajGen : public rclcpp::Node
{
  public:
    TrajGen():
      Node("traj_gen")
      {
        // Subscribe to the /nxt_pose topic
        sub_ = this->create_subscription<ros-2-planar-robot-acm::msg::RefPose>(
            "/nxt_pose",
            10,
            std::bind(&TrajGen::refPoseCallback, this, _1));

        // Advertise on the /desired_state topic
        pub_ = this->create_publisher<ros-2-planar-robot-acm::msg::CartesianState>("/desired_state", 10);
      }
 
  private:
    // Callback function for handling RefPose messages
    void refPoseCallback(const ros-2-planar-robot-acm::msg::RefPose::SharedPtr msg)
    {
        // Check if this is the first message
        if (!is_initialized_)
        {
            // Initialize the trajectory generator with the first pose
            initial_pose_ = Eigen::Vector2d(msg->x, msg->y);
            is_initialized_ = true;
            return;
        }

        // Update the trajectory with the new pose
        Eigen::Vector2d final_pose(msg->x, msg->y);
        traj_generator_.update(initial_pose_, final_pose, msg->deltat);

        // Stream the trajectory to the controller
        streamToController();
    }

    // Stream the current trajectory to the controller
    void streamToController()
    {
        // Create a CartesianState message
        auto msg = std::make_unique<ros-2-planar-robot-acm::msg::CartesianState>();
        msg->x = traj_generator_.X(0.0)(0);
        msg->y = traj_generator_.X(0.0)(1);
        msg->vx = traj_generator_.dX(0.0)(0);
        msg->vy = traj_generator_.dX(0.0)(1);

        // Publish the message
        pub_->publish(std::move(msg));
    }

    // Trajectory generator instance
    Point2Point traj_generator_;

    // Flag to check if the node is initialized
    bool is_initialized_{false};

    // Initial pose for the trajectory generator
    Eigen::Vector2d initial_pose_;

    // Subscriber and publisher
    rclcpp::Subscription<ros-2-planar-robot-acm::msg::RefPose>::SharedPtr sub_;
    rclcpp::Publisher<ros-2-planar-robot-acm::msg::CartesianState>::SharedPtr pub_;
};
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<TrajGen>());  
  rclcpp::shutdown();
  return 0;
}